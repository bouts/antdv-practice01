const RESP = {}
Object.defineProperties(RESP, {
  'SUCCESS': {
    value: 0,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'FAIL': {
    value: 1,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'ALREADY_LOGIN': {
    value: 2,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'NOT_LOGIN': {
    value: 3,
    writable: false,
    enumerable: true,
    configurable: true
  },
})

const LOGIN_TYPE = {}
Object.defineProperties(LOGIN_TYPE, {
  'ACCOUNT': {
    value: 0,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'QRCODE': {
    value: 1,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'REGISTER': {
    value: 2,
    writable: false,
    enumerable: true,
    configurable: true
  },
})

const USER_STATUS = {}
Object.defineProperties(USER_STATUS, {
  'ACTIVE': {
    value: 0,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'VALIDATION': {
    value: 1,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'FROZEN': {
    value: 2,
    writable: false,
    enumerable: true,
    configurable: true
  },
  'DELETED': {
    value: 3,
    writable: false,
    enumerable: true,
    configurable: true
  },
})

export default {
  RESP,
  LOGIN_TYPE,
  USER_STATUS,
}