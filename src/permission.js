import router from './router'
import {message} from 'ant-design-vue'

// 免登录白名单
const whiteList = [
  '/',            // 首页
  '/login'        // 登录界面
]

router.beforeEach((to, from, next) => {
  if (localStorage.getItem('token')) {
    // 登录过
    if (to.path === '/login') {
      message.info('您已经登录过')
      router.push('/').then(_ => {}).catch(_ => {})
    } else {
      next()
    }
  } else {
    // 没有登录过
    if (whiteList.indexOf(to.path) !== -1) {
      // 地址在白名单中，可以直接访问
      next()
    } else {
      // 否则需要进入登录界面
      next({
        path: '/login'
      })
    }
  }
})