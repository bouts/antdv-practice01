let isDevelopment = process.env.NODE_ENV === 'development'
module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: '酷冷不酷冷 - 做点有意思的事情'
    }
  },
  devServer: {
    port: 3000,
    https: !isDevelopment,
    disableHostCheck: true,
  }
}