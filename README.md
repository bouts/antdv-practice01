# 1. 运行依赖

- 请在根路径下(`vue.config.js`同级目录)新增至少两个文件: 

  - `.env.development`: 开发环境配置
  - `.env.production`: 生产环境配置
  
- 配置参数解释：

  - `VUE_APP_BASE_URL`: 配置接口访问前缀

# 2. Todo List

- [x] 二维码文件扫码
- [x] 摄像头扫码
- [x] 下线同账号不同设备功能
- [ ] 前端埋点

# antdv-practice01

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
